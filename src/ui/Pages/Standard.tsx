import React, { useEffect, useState } from 'react';

import { notification } from 'antd';

import { Standard as StandardModel } from '../../domain/standard/model';
import * as service from '../../domain/standard/service';
import { PaginatedResult } from '../../domain/common/paginated-result';

import { Action } from '../Components/Common/ActionsMenu';
import StandardForm from '../Components/Standard/Form';
import StandardTable from '../Components/Standard/Table';

const Standard = () => {
    const [standards, setStandards] = useState<StandardModel[]>([]);

    const [currentStandard, setCurrentStandard] = useState<StandardModel>();

    useEffect(() => {
        (async () => {
            const standards_ = await service.list(0, 10);

            setStandards((standards_ as PaginatedResult<StandardModel>).items);
        })();
    }, []);

    async function onFinish(standard: StandardModel): Promise<void> {
        try {
            if (currentStandard) {
                await service.update(standard.code, standard);

                setStandards([...standards.filter(s => s.code != standard.code), standard]);
            } else {
                await service.create(standard);

                setStandards([...standards, standard]);
            }

            setCurrentStandard(undefined);

            notification.success({
                message: 'Norma salva com sucesso!'
            });
        } catch (error) {
            notification.error({
                message: 'Oops, parece que algo deu errado =('
            });
        }
    }

    function onDropdownClick(action: Action, standard: StandardModel) {
        (async () => {
            try {
                if (action === Action.EDIT) {
                    setCurrentStandard(standard);
                } else if (action === Action.DELETE) {
                    await service.remove(standard.code);

                    setStandards(standards.filter(s => s.code != standard.code));

                    notification.success({
                        message: 'Ação finalizada com sucesso!'
                    });
                }
            } catch (error) {
                notification.error({
                    message: 'Oops, parece que algo deu errado =('
                });
            }
        })();
    }

    return (
        <div>
            <h1 style={{ textAlign: 'center' }}>Gestão de normas</h1>

            <div style={{ margin: 50 }}>
                <StandardForm standard={currentStandard} onFinish={onFinish} />
                <StandardTable standards={standards} onDropdownClick={onDropdownClick} />
            </div>
        </div>
    );
}

export default Standard;
