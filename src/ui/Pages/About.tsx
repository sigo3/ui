import React, { useCallback } from 'react';

import { Button } from 'antd';
import { useKeycloak } from '@react-keycloak/web';

const About = () => {
    const { keycloak } = useKeycloak();

    const login = useCallback(() => {
        keycloak?.login()
    }, [keycloak]);

    return (
        <div style={{ padding: 20, width: '100%' }}>
            <h1 style={{ textAlign: 'center' }}>Sobre</h1>
            <div>
                <p>O SIGO é uma plataforma de gestão de processos têxtis desenvolvido para a empresa fictícia IndTexBr Indústria Têxtil do Brasil SA, como parte do trabalho de conclusão de curso da pós graduação em arquitetura de sistemas distribuídos da pucminas.</p>
                <Button style={{ width: '100%' }} key="submit" type="primary" onClick={login}>Entrar</Button>
            </div >
        </div>
    );
}

export default About;
