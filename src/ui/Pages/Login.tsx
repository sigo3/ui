import * as React from 'react'
import { useCallback } from 'react'

import { Redirect, useHistory, useLocation } from 'react-router-dom'

import { useKeycloak } from '@react-keycloak/web'
import { Button, Modal } from 'antd'

const Login = () => {
    const location = useLocation<{ [key: string]: unknown }>();
    const history = useHistory();

    const currentLocation = location.state || {
        from: { pathname: '/' }
    };

    const { keycloak } = useKeycloak();

    const login = useCallback(() => {
        keycloak?.login()
    }, [keycloak]);

    if (keycloak?.authenticated) {
        return <Redirect to={currentLocation.from as string} />;
    }

    return (
        <Modal
            visible={!keycloak?.authenticated}
            title="Autenticação"
            closable={false}
            footer={[
                <Button key="about" type="primary" onClick={() => history.push('/about')}>Sobre</Button>,
                <Button key="submit" type="primary" onClick={login}>Entrar</Button>
            ]}
        >
            <p>Parece que você não está autenticado.</p>
            <p>Clique em Entrar para autenticar</p>
        </Modal>
    )
}

export default Login;