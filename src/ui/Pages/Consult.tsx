import React, { useEffect, useState } from 'react';

import { notification } from 'antd';

import { Consult as ConsultModel } from '../../domain/consult/model';
import { Standard as StandardModel } from '../../domain/standard/model';

import { PaginatedResult } from '../../domain/common/paginated-result';

import * as consultService from '../../domain/consult/service';
import * as standardService from '../../domain/standard/service';

import { Action } from '../Components/Common/ActionsMenu';
import ConsultForm from '../Components/Consult/Form';
import ConsultTable from '../Components/Consult/Table';

const Consult = () => {
    const [consults, setConsults] = useState<ConsultModel[]>([]);
    const [standards, setStandards] = useState<StandardModel[]>([]);

    const [currentConsult, setCurrentConsult] = useState<ConsultModel>();

    useEffect(() => {
        (async () => {
            const consults_ = await consultService.list(0, 10);

            setConsults((consults_ as PaginatedResult<ConsultModel>).items);

            const standards_ = await standardService.list(0, 10);

            setStandards((standards_ as PaginatedResult<StandardModel>).items);
        })();
    }, []);

    async function onFinish(consult: ConsultModel): Promise<void> {
        try {
            consult.standard = standards.find(s => s.code === consult.standardCode) as StandardModel;
            if (currentConsult) {
                await consultService.update(consult.code, consult);

                setConsults([...consults.filter(s => s.code != consult.code), consult]);
            } else {
                await consultService.create(consult);

                setConsults([...consults, consult]);
            }

            setCurrentConsult(undefined);

            notification.success({
                message: 'Consultoria salva com sucesso!'
            });
        } catch (error) {
            notification.error({
                message: 'Oops, parece que algo deu errado =('
            });
        }
    }

    function onDropdownClick(action: Action, consult: ConsultModel) {
        (async () => {
            try {
                if (action === Action.EDIT) {
                    setCurrentConsult(consult);
                } else if (action === Action.DELETE) {
                    await consultService.remove(consult.code);

                    setConsults(consults.filter(s => s.code != consult.code));

                    notification.success({
                        message: 'Ação finalizada com sucesso!'
                    });
                }
            } catch (error) {
                notification.error({
                    message: 'Oops, parece que algo deu errado =('
                });
            }
        })();
    }

    return (
        <div>
            <h1 style={{ textAlign: 'center' }}>Consultoria e assessoria</h1>

            <div style={{ margin: 50 }}>
                <ConsultForm consult={currentConsult} standards={standards} onFinish={onFinish} />
                <ConsultTable consults={consults} onDropdownClick={onDropdownClick} />
            </div>
        </div>
    );
}

export default Consult;
