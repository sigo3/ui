import React from 'react';

const Home = () => {
    return (
        <div className='layout-flex'>
            <h1>Bem vindo ao SIGO - Sistema Integrado de Gestão e Operação</h1>
        </div >
    );
}

export default Home;
