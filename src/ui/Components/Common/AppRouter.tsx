import * as React from 'react'

import { Route, Redirect, RouteComponentProps } from 'react-router-dom'
import type { RouteProps } from 'react-router-dom'

import { useKeycloak } from '@react-keycloak/web'

import { isAuthorized } from '../../../keycloak';

const PrivateRoute = ({ component: Component, roles, ...rest }: PrivateRouteParams) => {
    const { keycloak } = useKeycloak();

    return (
        <Route {...rest}
            render={props => isAuthorized(keycloak, roles)
                ? <Component {...props} />
                : <Redirect
                    to={{
                        pathname: '/login',
                        state: { from: props.location }
                    }}
                />
            }
        />
    );
}

export default PrivateRoute;

export type PrivateRouteParams = {
    component:
    | React.ComponentType<RouteComponentProps<any>>
    | React.ComponentType<any>;
    roles?: string[]
} & RouteProps;
