import React from 'react';

import { Link } from 'react-router-dom';

import { Button, Menu } from 'antd';
import { useKeycloak } from '@react-keycloak/web';

import { isAuthorized } from '../../../keycloak';

const AppMenu = () => {
    const { keycloak } = useKeycloak();

    if (!keycloak?.authenticated) {
        return null;
    }

    return (
        <Menu theme='dark' mode='horizontal' defaultSelectedKeys={['1']}>
            <Menu.Item key='home'>
                <Link to='/'>Inicial</Link>
            </Menu.Item>

            <Menu.Item key='standards'>
                <Link to='/standards'>Gestão de Normas</Link>
            </Menu.Item>

            {
                isAuthorized(keycloak, ['admin'])
                    ?
                    <Menu.Item key='consults'>
                        <Link to='/consults'>Consultoria e Assessoria</Link>
                    </Menu.Item>
                    : null
            }

            <Menu.Item key='logout' style={{ float: 'right' }}>
                <Button type='ghost' style={{ color: '#fff', border: 'none' }} onClick={() => keycloak.logout()}>
                    Sair
                </Button>
            </Menu.Item>
        </Menu >
    );
}

export default AppMenu;