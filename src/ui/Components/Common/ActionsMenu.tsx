import * as React from 'react';

import { Dropdown, Menu, Button } from 'antd';

import { BarsOutlined, DownOutlined } from '@ant-design/icons';

const ActionsMenu = ({ actions, onClick }: ActionsMenuProps) => (
    <Dropdown
        overlay={
            <Menu onClick={(param): void => onClick(param.key as Action)}>
                {actions.map(({ action, title }) => (
                    <Menu.Item key={action}>{title}</Menu.Item>
                ))}
            </Menu>
        }
    >
        <Button>
            <BarsOutlined />
            <DownOutlined />
        </Button>
    </Dropdown>
);

export default ActionsMenu;

export type ActionsMenuProps = {
    onClick: (action: Action) => void;
    actions: {
        action: Action;
        title: string;
    }[];
};

export enum Action {
    DELETE = 'DELETE',
    EDIT = 'EDIT'
}