import React from 'react';

import { ReactKeycloakProvider } from '@react-keycloak/web'

import { Layout } from 'antd';

import 'antd/dist/antd.css';

import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';

import keycloak from '../../keycloak';

import Home from '../Pages/Home';
import Standard from '../Pages/Standard';
import Consult from '../Pages/Consult';
import About from '../Pages/About';
import Login from '../Pages/Login';

import PrivateRoute from './Common/AppRouter';
import AppMenu from './Common/AppMenu';

import './App.css';

const App = () => {
  return (
    <ReactKeycloakProvider authClient={keycloak}>
      <Router>
        <Layout className="layout">
          <Layout.Header>
            <AppMenu />
          </Layout.Header>

          <Layout.Content>
            <div className="layout-content">
              <Switch>
                <PrivateRoute path='/' exact={true} component={Home} />
                <PrivateRoute path='/standards' exact={true} component={Standard} />
                <PrivateRoute roles={['admin']} path='/consults' exact={true} component={Consult} />
                <Route path='/login' exact={true} component={Login} />
                <Route path='/about' exact={true} component={About} />
              </Switch>
            </div>
          </Layout.Content>

          <Layout.Footer style={{ textAlign: 'center' }} className='layout-footer'>
            SIGO {new Date().getFullYear()} - Desenvolvido por Guilherme Vasconcellos
      </Layout.Footer>
        </Layout>
      </Router>
    </ReactKeycloakProvider>
  );
}

export default App;
