import React from 'react';

import moment from 'moment';

import { Empty, Modal, Table } from 'antd';
import { ColumnsType } from 'antd/lib/table';

import { StandardSource, StandardStatus } from '../../../domain/standard/enum';
import { Standard } from '../../../domain/standard/model';
import ActionsMenu, { Action } from '../Common/ActionsMenu';

const statusMap = {
    [StandardStatus.NotOperative]: 'Fora de vigência',
    [StandardStatus.Operative]: 'Vigente',
    [StandardStatus.Outdated]: 'Depreciado',
    [StandardStatus.PendingApproval]: 'Em aprovação'
};

const sourceMap = {
    [StandardSource.Internal]: 'Interno',
    [StandardSource.External]: 'Externo'
};


function getColumns(onDropdownClick: DropdownClickAction): ColumnsType<Standard> {
    const columns: ColumnsType<Standard> = [
        {
            title: 'Código',
            dataIndex: 'code',
        },
        {
            title: 'Título',
            dataIndex: 'title',
        },
        {
            title: 'Descrição',
            dataIndex: 'description',
        },
        {
            title: 'Comitê',
            dataIndex: 'committee',
        },
        {
            title: 'Documento',
            dataIndex: 'url',
            render(_, standard) {
                const url = standard.url.toLowerCase().includes('http') ? standard.url : `http://${standard.url}`;

                return <a href={url} target='_blank' rel='noopener noreferrer'>{standard.title}</a>
            }
        },
        {
            title: 'Data de publicação',
            dataIndex: 'publishDate',
            render(_, standard) {
                return moment(standard.publishDate).format('MM/YYYY');
            }
        },
        {
            title: 'Situação',
            dataIndex: 'status',
            render(_, standard) {
                return statusMap[standard.status];
            }
        },
        {
            title: 'Origem do documento',
            dataIndex: 'source',
            render(_, standard) {
                return sourceMap[standard.source];
            }
        },
        {
            title: 'Ações',
            key: 'actions',
            fixed: 'right',
            render: (standard: Standard) => (
                <ActionsMenu
                    onClick={(action: Action): void => {
                        if (action === Action.DELETE) {
                            Modal.confirm({
                                title: `Tem certeza que deseja remover ${standard.title}?`,
                                onOk: () => onDropdownClick(action, standard)
                            });

                            return;
                        }

                        onDropdownClick(action, standard);
                    }}
                    actions={[
                        {
                            action: Action.EDIT,
                            title: 'Editar'
                        },
                        {
                            action: Action.DELETE,
                            title: 'Remover'
                        }
                    ]}
                />
            )
        }
    ];

    return columns;
}

const StandardTable = ({ standards, onDropdownClick }: StandardTableProps) => {
    return <Table
        locale={{
            emptyText: <Empty description="Nenhuma norma cadastrada" />
        }}
        rowKey="code"
        pagination={false}
        dataSource={standards}
        columns={getColumns(onDropdownClick)}
    />
}

export default StandardTable;

export type DropdownClickAction = (action: Action, standard: Standard) => void;

export type StandardTableProps = {
    standards?: Standard[];
    onDropdownClick: DropdownClickAction;
}
