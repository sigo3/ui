import React from 'react';

import moment from 'moment';

import { Button, DatePicker, Form, Input, Radio, } from 'antd';
import { Rule } from 'antd/lib/form';

import { StandardSource, StandardStatus } from '../../../domain/standard/enum';
import { Standard } from '../../../domain/standard/model';

const rules: Rule[] = [
  {
    required: true
  }
];

const StandardForm = ({ standard, onFinish }: StandardFormProps) => {
  const [form] = Form.useForm();

  if (standard) {
    form.setFieldsValue({
      ...standard,
      publishDate: moment(standard.publishDate)
    });
  }

  return (
    <Form form={form} onFinish={async (standard: Standard): Promise<void> => {
      await onFinish(standard);
      form.resetFields();
    }} wrapperCol={{ span: 8 }} labelCol={{ span: 8 }}>
      <Form.Item name="code" label="Código" rules={rules}>
        <Input />
      </Form.Item>

      <Form.Item name="title" label="Título" rules={rules}>
        <Input />
      </Form.Item>

      <Form.Item name="description" label="Descrição" rules={rules}>
        <Input />
      </Form.Item>

      <Form.Item name="committee" label="Comitê" rules={rules}>
        <Input />
      </Form.Item>

      <Form.Item name="url" label="Url" rules={rules}>
        <Input />
      </Form.Item>

      <Form.Item name="publishDate" label="Data de publicação" rules={rules}>
        <DatePicker />
      </Form.Item>

      <Form.Item name="status" label="Situação" rules={rules}>
        <Radio.Group>
          <Radio value={StandardStatus.NotOperative}>Fora de vigência</Radio>
          <Radio value={StandardStatus.Operative}>Vigente</Radio>
          <Radio value={StandardStatus.Outdated}>Depreciado</Radio>
          <Radio value={StandardStatus.PendingApproval}>Em aprovação</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item name="source" label="Origem" rules={rules}>
        <Radio.Group>
          <Radio value={StandardSource.Internal}>Documento interno</Radio>
          <Radio value={StandardSource.External}>Documento externo</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item wrapperCol={{ offset: 8, span: 8 }}>
        <Button type="primary" htmlType="submit">
          Salvar
        </Button>

        <Button htmlType="button" onClick={(): void => {
          form.resetFields();
        }}>
          Limpar
        </Button>
      </Form.Item>
    </Form>
  );
}

export default StandardForm;

export type StandardFormProps = {
  standard?: Standard;
  onFinish(standard: Standard): Promise<void>;
}
