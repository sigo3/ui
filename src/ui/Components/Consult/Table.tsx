import React from 'react';

import { Empty, Modal, Table } from 'antd';
import { ColumnsType } from 'antd/lib/table';

import { Consult } from '../../../domain/consult/model';
import ActionsMenu, { Action } from '../Common/ActionsMenu';

function getColumns(onDropdownClick: DropdownClickAction): ColumnsType<Consult> {
    const columns: ColumnsType<Consult> = [
        {
            title: 'Código',
            dataIndex: 'code',
        },
        {
            title: 'Empresa',
            dataIndex: 'company'
        },
        {
            title: 'Descrição',
            dataIndex: 'description'
        },
        {
            title: 'Situação',
            dataIndex: 'status',
        },
        {
            title: 'Norma associada',
            dataIndex: 'standard',
            render(_, consult) {
                return `${consult.standard?.code  ?? ''} - ${consult.standard?.title ?? ''}`
            }
        },
        {
            title: 'Ações',
            key: 'actions',
            fixed: 'right',
            render: (consult: Consult) => (
                <ActionsMenu
                    onClick={(action: Action): void => {
                        if (action === Action.DELETE) {
                            Modal.confirm({
                                title: `Tem certeza que deseja remover ${consult.company}?`,
                                onOk: () => onDropdownClick(action, consult)
                            });

                            return;
                        }

                        onDropdownClick(action, consult);
                    }}
                    actions={[
                        {
                            action: Action.EDIT,
                            title: 'Editar'
                        },
                        {
                            action: Action.DELETE,
                            title: 'Remover'
                        }
                    ]}
                />
            )
        }
    ];

    return columns;
}

const ConsultTable = ({ consults, onDropdownClick }: ConsultTableProps) => {
    return <Table
        locale={{
            emptyText: <Empty description="Nenhuma norma cadastrada" />
        }}
        rowKey="code"
        pagination={false}
        dataSource={consults}
        columns={getColumns(onDropdownClick)}
    />
}

export default ConsultTable;

export type DropdownClickAction = (action: Action, consult: Consult) => void;

export type ConsultTableProps = {
    consults?: Consult[];
    onDropdownClick: DropdownClickAction;
}
