import React from 'react';

import { Button, Form, Input, Radio, Select, } from 'antd';
import { Rule } from 'antd/lib/form';

import { ConsultStatus } from '../../../domain/consult/enum';
import { Consult } from '../../../domain/consult/model';
import { Standard } from '../../../domain/standard/model';

const rules: Rule[] = [
  {
    required: true
  }
];

const ConsultForm = ({ consult, standards, onFinish }: ConsultFormProps) => {
  const [form] = Form.useForm();

  if (consult) {
    form.setFieldsValue(consult);
  }

  return (
    <Form form={form} onFinish={async (consult: Consult): Promise<void> => {
      await onFinish(consult);
      form.resetFields();
    }} wrapperCol={{ span: 8 }} labelCol={{ span: 8 }}>
      <Form.Item name="code" label="Código" rules={rules}>
        <Input />
      </Form.Item>

      <Form.Item name="company" label="Empresa" rules={rules}>
        <Input />
      </Form.Item>

      <Form.Item name="description" label="Descrição" rules={rules}>
        <Input />
      </Form.Item>

      <Form.Item name="status" label="Situação" rules={rules}>
        <Radio.Group>
          <Radio style={{ display: 'block' }} value={ConsultStatus.Pending}>Pendente</Radio>
          <Radio style={{ display: 'block' }} value={ConsultStatus.InProgress}>Em andamento</Radio>
          <Radio style={{ display: 'block' }} value={ConsultStatus.Finished}>Finalizada</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item name="standardCode" label="Norma associada" rules={rules}>
        <Select placeholder="Selecione a norma a ser associada">
          {
            standards.map(standard => (
              <Select.Option key={standard.code} value={standard.code}>{standard.code} - {standard.title}</Select.Option>
            ))
          }
        </Select>
      </Form.Item>

      <Form.Item wrapperCol={{ offset: 8, span: 8 }}>
        <Button type="primary" htmlType="submit">
          Salvar
        </Button>

        <Button htmlType="button" onClick={(): void => {
          form.resetFields();
        }}>
          Limpar
        </Button>
      </Form.Item>
    </Form>
  );
}

export default ConsultForm;

export type ConsultFormProps = {
  consult?: Consult;
  standards: Standard[];
  onFinish(consult: Consult): Promise<void>;
}
