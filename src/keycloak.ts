import Keycloak, { KeycloakInstance } from 'keycloak-js';

export default Keycloak({
    url: 'http://localhost:8080/auth',
    realm: 'SIGO',
    clientId: 'sigo-ui',
});

export const isAuthorized = (keycloak: KeycloakInstance, roles: string[] = []) => {
    if (!keycloak?.authenticated) {
        return false;
    }

    if (!roles.length) {
        return true;
    }

    return roles.some(r => {
        const realm = keycloak.hasRealmRole(r);
        const resource = keycloak.hasResourceRole(r);

        return realm || resource;
    });
}
