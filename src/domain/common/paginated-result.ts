export type PaginatedResult<TModel> = {
    items: TModel[];
    totalCount: number;
    totalPages: number;
    page: number;
    pageSize: number;
}
