export enum ConsultStatus {
    Pending = 0,
    InProgress = 1,
    Finished = 2
}