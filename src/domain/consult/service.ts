import axios, { AxiosError, AxiosResponse } from 'axios';

import { PaginatedResult } from '../common/paginated-result';
import { Consult } from './model';

const host = 'http://localhost:5001';
const params = {};

export async function create(consult: Consult) {
    try {
        const endpoint = `${host}/consult`;
        const response: AxiosResponse<Consult> = await axios.post(endpoint, consult, { ...params });

        return response.data;
    } catch (err) {
        if ((err as AxiosError).isAxiosError) {
            throw new Error((err as AxiosError)?.response?.data.message);
        }
    }
}

export async function update(code: string, consult: Consult) {
    try {
        const endpoint = `${host}/consult/${code}`;
        const response: AxiosResponse = await axios.put(endpoint, consult, { ...params });

        return response.data;
    } catch (err) {
        if ((err as AxiosError).isAxiosError) {
            throw new Error((err as AxiosError)?.response?.data.message);
        }
    }
}

export async function find(code: string) {
    try {
        const endpoint = `${host}/consult/${code}`;
        const response: AxiosResponse<Consult> = await axios.get(endpoint, { ...params });

        return response.data;
    } catch (err) {
        if ((err as AxiosError).isAxiosError) {
            throw new Error((err as AxiosError)?.response?.data.message);
        }
    }
}

export async function list(page: number, pageSize: number) {
    try {
        const endpoint = `${host}/consult`;
        const response: AxiosResponse<PaginatedResult<Consult>> = await axios.get(endpoint, {
            ...params,
            params: {
                page,
                pageSize
            }
        });

        return response.data;
    } catch (err) {
        if ((err as AxiosError).isAxiosError) {
            throw new Error((err as AxiosError)?.response?.data.message);
        }
    }
}

export async function remove(code: string) {
    try {
        const endpoint = `${host}/consult/${code}`;
        const response: AxiosResponse = await axios.delete(endpoint, { ...params });

        return response.data;
    } catch (err) {
        if ((err as AxiosError).isAxiosError) {
            throw new Error((err as AxiosError)?.response?.data.message);
        }
    }
}
