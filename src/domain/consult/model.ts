import { ConsultStatus } from './enum';

import { Standard } from '../standard/model';

export type Consult = {
    code: string;
    company: string;
    description: string;
    status: ConsultStatus;
    standardCode: string;
    standard: Standard;
}
