import axios, { AxiosError, AxiosResponse } from 'axios';

import { PaginatedResult } from '../common/paginated-result';
import { Standard } from './model';

const host = 'http://localhost:5000';
const params = {};

export async function create(standard: Standard) {
    try {
        const endpoint = `${host}/standard`;
        const response: AxiosResponse<Standard> = await axios.post(endpoint, standard, { ...params });

        return response.data;
    } catch (err) {
        if ((err as AxiosError).isAxiosError) {
            throw new Error((err as AxiosError)?.response?.data.message);
        }
    }
}

export async function update(code: string, standard: Standard) {
    try {
        const endpoint = `${host}/standard/${code}`;
        const response: AxiosResponse = await axios.put(endpoint, standard, { ...params });

        return response.data;
    } catch (err) {
        if ((err as AxiosError).isAxiosError) {
            throw new Error((err as AxiosError)?.response?.data.message);
        }
    }
}

export async function find(code: string) {
    try {
        const endpoint = `${host}/standard/${code}`;
        const response: AxiosResponse<Standard> = await axios.get(endpoint, { ...params });

        return response.data;
    } catch (err) {
        if ((err as AxiosError).isAxiosError) {
            throw new Error((err as AxiosError)?.response?.data.message);
        }
    }
}

export async function list(page: number, pageSize: number) {
    try {
        const endpoint = `${host}/standard`;
        const response: AxiosResponse<PaginatedResult<Standard>> = await axios.get(endpoint, {
            ...params,
            params: {
                page,
                pageSize
            }
        });

        return response.data;
    } catch (err) {
        if ((err as AxiosError).isAxiosError) {
            throw new Error((err as AxiosError)?.response?.data.message);
        }
    }
}

export async function remove(code: string) {
    try {
        const endpoint = `${host}/standard/${code}`;
        const response: AxiosResponse = await axios.delete(endpoint, { ...params });

        return response.data;
    } catch (err) {
        if ((err as AxiosError).isAxiosError) {
            throw new Error((err as AxiosError)?.response?.data.message);
        }
    }
}
