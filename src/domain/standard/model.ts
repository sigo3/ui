import { StandardSource, StandardStatus } from './enum';

export type Standard = {
    code: string
    title: string
    description: string
    committee: string
    url: string
    publishDate: Date
    status: StandardStatus
    source: StandardSource
}
