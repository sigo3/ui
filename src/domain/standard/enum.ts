export enum StandardStatus {
    PendingApproval = 0,
    Outdated = 1,
    Operative = 2,
    NotOperative = 3
}

export enum StandardSource {
    Internal = 0,
    External = 1
}
